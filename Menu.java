package UTP;
public class Menu {
    private String nama;
    private int harga;

    // konstruktor dengan parameter nama dan harga
    public Menu(String nama, int harga) {
        this.nama = nama;
        this.harga = harga;
    }
    // method getter digunakan untuk mengembalikan nilai dari atribut nama
    public String getNama() {
        return nama;
    }
    // method setter digunakan untuk mengubah nilai dari atribut nama
    public void setNama(String nama) {
        this.nama = nama;
    }
    // method getter digunakan untuk mengembalikan nilai dari atribut harga
    public int getHarga() {
        return harga;
    }
    // method setter digunakan untuk mengubah nilai dari atribut harga
    public void setHarga(int harga) {
        this.harga = harga;
    }
}
