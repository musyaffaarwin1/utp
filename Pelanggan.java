package UTP;
public class Pelanggan {
    private String nama;

    // konstruktor dengan parameter nama
    public Pelanggan(String nama) {
        this.nama = nama;
    }
    // method getter digunakan untuk mengembalikan nilai dari atribut nama
    public String getNama() {
        return nama;
    }
    // method setter digunakan untuk mengubah nilai dari atribut nama
    public void setNama(String nama) {
        this.nama = nama;
    }
}
/* Dengan menggunakan access  modifier private pada atribut nama
maka objek dari class pelanggan yang dapat mengakses dan mengubah nilai dari atribut tersebut.
 */